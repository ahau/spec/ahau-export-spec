# ahau export import

## Whakapapa Export/ Import

When exporting a whakapapa from Ahau, several sort of records are captured:
- `profile/person`
    - this the merging of the group profile and admin profile (if you have access) for each person in the whakapapa
- `link/profile-profile/child`
    - this includes meta-data `relationshipType` and `legallyAdopted`
- `link/profile-profile/partner`

Data that is currently NOT exported:
- the author of records
- timestamps (e.g. date of creation)
- edit history of records
- `whakapapa/view` data
    - `title`, `description`, `avatarImage`
    - `focus` (where this whakapapa starts)
    - `importantRelationships`, `ignoredProfiles` (extra rules for drawing)

NOTE:
By default we do not export any profile listed in `ignoredProfiles`


### CSV format

```mermaid
graph TB

James-->Tama-->Sarah
       Pania-->Sarah

Tama-.-Pania
```

```csv
id, parentId, relationshipType, legallyAdopted, preferredName, .....
1 ,         ,                 ,               , James        , 
2 , 1       , whangai         , no            , Tama         , 
3 , 2       , partner         ,               , Pania        , 
4 , 2       , parent          ,               , Sarah        , 
4 , 3       , parent          ,               ,              , 
```

NOTES:
- it's acceptable to list a row which just specifies link data (the first 4 columns)
- id fields `id`, `parentId` are treated as Strings
- child relationships are mapped from database to CSV format like: 
    - database
        ```js
        {
          type: 'link/profile-profile/child',
          parent: 1,
          child: 2,
          relationshipType: 'birth',
          legallyAdopted: true
        }
        ```
    - CSV
        ```csv
        id, parentId, relationshipType, legallyAdopted
        2 , 1       , birth           , yes           
        ```
        - NOTE booleans are mapped to `yes|no` for accessibility

- partner relationships are mapped from database to CSV format like: 
    - database
        ```js
        {
          type: 'link/profile-profile/partner',
          parent: 31,
          child: 45
        }
        ```
    - CSV
        ```csv
        id, parentId, relationshipType, legallyAdopted
        31, 45      , partner         ,
        ```
        - NOTE here we have re-used use `parentId` field and `relationshipType` fields
        - partner links are undirected, so it doesn't matter which is `id`/ `parentId`
   
- fields which have an array format (e.g. `altNames` use `||` as a delimiter beteween entries)
- image fields `avatarImage`, `headerImage` store data in a URI format:
    - `ssb:blob/classic/<key>?mimeType=<mimeType>&unbox=<unbox>`
    - NOTE that [`ssb-uri-spec`](https://github.com/ssb-ngi-pointer/ssb-uri-spec) specifices URI encodeing of base64 as:
        > equivalent to Base64 where `+` characters are replaced with `-`, and `/` characters are replaced with `_`.
    - the `key` here doesn't include prefix or suffic (`&` nor `sha256`)
    - see `ssb-uri2` and `ssb-bfe` for tools
